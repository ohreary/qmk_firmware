#include "iris.h"
#include "action_layer.h"
#include "eeconfig.h"
#include "mousekey.h"
#include "process_unicode.h"
extern keymap_config_t keymap_config;

#define _QWERTY 0
#define _NAVIGATION 1
#define _CODE 2
#define _CONSTANTS 3
#define _ADJUST 16

enum custom_keycodes {
  QWERTY = SAFE_RANGE,
  LOWER,
  RAISE,
  ADJUST,
  CODE,
  END_CONSTANT,
  RASP,
  LOSP,
  LOCO,
  TABB,
  BKAD,
  FLIP,
  TSET,
  SRUG,
  DISA
};

#define KC_ KC_TRNS
#define _______ KC_TRNS

// Keycode names
#define KC_RSEN MT(MOD_RSFT, KC_ENT) // Tap for enter, hold for right shift
#define KC_RALD MT(MOD_RALT, KC_DEL) // Tap for delete, hold for right alt

#define KC_LCCA MT(MOD_LCTL, KC_CAPS)  // Tap for caps lock, hold for left control
#define KC_SHDE MT(MOD_LSFT, KC_DEL) // Tap for delete, hold for left shift
#define KC_ALEN MT(MOD_LALT, KC_ENT) // Tap for enter, hold for left alt

#define KC_CONSTANTS TO(_CONSTANTS)
#define KC_DEF TO(_QWERTY)

#define KC_ECON END_CONSTANT

#define KC_ADJT ADJUST
#define KC_RASP RASP
#define KC_LOSP LOSP
#define KC_LOCO LOCO
#define KC_TABB TABB
#define KC_BKAD BKAD
#define KC_FLIP FLIP
#define KC_TSET TSET
#define KC_SRUG SRUG
#define KC_DISA DISA
#define KC_RST RESET
#define KC_BL_S BL_STEP

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

  [_QWERTY] = LAYOUT_kc(
  //,----+----+----+----+----+----.              ,----+----+----+----+----+----.
     ESC , 1  , 2  , 3  , 4  , 5  ,                6  , 7  , 8  , 9  , 0  , GRV,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
     TAB, Q  , W  , E  , R  , T  ,                Y  , U  , I  , O  , P  ,BSPC,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
     LCCA, A  , S  , D  , F  , G  ,                H  , J  , K  , L  ,SCLN,QUOT,
  //|----+----+----+----+----+----+----.    ,----|----+----+----+----+----+----|
     SHDE, Z  , X  , C  , V  , B  ,LGUI,     RALD, N  , M  ,COMM,DOT ,SLSH,RSEN,
  //`----+----+----+--+-+----+----+----/    \----+----+----+----+----+----+----'
                       ALEN,RASP,LOCO,         LOSP,RASP,RCTL
  //                  `----+----+----'        `----+----+----'
  ),

  [_NAVIGATION] = LAYOUT_kc(
  //,----+----+----+----+----+----.              ,----+----+----+----+----+----.
     TRNS, F1 , F2 , F3 , F4 , F5 ,                F6 , F7 , F8 , F9 , F10,TRNS,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
     TRNS,FLIP,    , UP ,    ,    ,               WH_U,BTN1,MS_U,BTN2,INS ,TRNS,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
     TRNS,TSET,LEFT,DOWN,RGHT,    ,               WH_D,MS_L,MS_D,MS_R,PSCR,TRNS,
  //|----+----+----+----+----+----+----.    ,----|----+----+----+----+----+----|
     TRNS,SRUG,DISA,    ,    ,BSPC,TRNS,     TRNS,    ,    ,    ,    ,    ,TRNS,
  //`----+----+----+--+-+----+----+----/    \----+----+----+----+----+----+----'
                       TRNS,TRNS,TRNS,         TRNS,TRNS,TRNS
  //                  `----+----+----'        `----+----+----'
  ),

  [_CODE] = LAYOUT_kc(
  //,----+----+----+----+----+----.              ,----+----+----+----+----+----.
     TRNS, F1 , F2 , F3 , F4 , F5 ,                F6 , F7 , F8 , F9 ,F10 ,DEL ,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
     TRNS,    ,    ,LCBR,RCBR,    ,                   ,AMPR,PIPE,    ,    ,BSLS,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
     TRNS,    ,    ,LPRN,RPRN,    ,                   ,UNDS,MINS,    ,    ,TRNS,
  //|----+----+----+----+----+----+----.    ,----|----+----+----+----+----+----|
     TRNS,    ,    ,LBRC,RBRC,BSPC,TRNS,     TRNS,    ,PLUS,EQL ,    ,    ,TRNS,
  //`----+----+----+--+-+----+----+----/    \----+----+----+----+----+----+----'
                       TRNS,TRNS,TRNS,         TRNS,TRNS,TRNS
  //                  `----+----+----'        `----+----+----'
  ),

  [_CONSTANTS] = LAYOUT(
  //,--------+--------+--------+--------+--------+--------.                          ,--------+--------+--------+--------+--------+--------.
       KC_DEF,  KC_DEF,  KC_DEF,  KC_DEF,  KC_DEF,  KC_DEF,                             KC_DEF,  KC_DEF,  KC_DEF,  KC_DEF,  KC_DEF,  KC_DEF,
  //|--------+--------+--------+--------+--------+--------|                          |--------+--------+--------+--------+--------+--------|
       KC_DEF, S(KC_Q), S(KC_W), S(KC_E), S(KC_R), S(KC_T),                            S(KC_Y), S(KC_U), S(KC_I), S(KC_O), S(KC_P), KC_BSPC,
  //|--------+--------+--------+--------+--------+--------|                          |--------+--------+--------+--------+--------+--------|
       KC_DEF, S(KC_A), S(KC_S), S(KC_D), S(KC_F), S(KC_G),                            S(KC_H), S(KC_J), S(KC_K), S(KC_L),  KC_DEF,  KC_DEF,
  //|--------+--------+--------+--------+--------+--------+--------.        ,--------|--------+--------+--------+--------+--------+--------|
       KC_DEL, S(KC_Z), S(KC_X), S(KC_C), S(KC_V), S(KC_B),  KC_DEF,           KC_DEF, S(KC_N), S(KC_M),  KC_DEF,  KC_DEF,  KC_DEF, KC_ECON,
  //`--------+--------+--------+----+---+--------+--------+--------/        \--------+--------+--------+---+----+--------+--------+--------'
                                      KC_UNDS, KC_UNDS, _______,                  KC_UNDS, KC_UNDS, KC_UNDS
  //                                `--------+--------+--------'                `--------+--------+--------'
  ),

  [_ADJUST] = LAYOUT(
  //,--------+--------+--------+--------+--------+--------.                          ,--------+--------+--------+--------+--------+--------.
      _______, RGB_TOG, BL_STEP, _______, _______, RESET  ,                            _______, _______, _______, KC_SLEP, KC_PWR, _______,
  //|--------+--------+--------+--------+--------+--------|                          |--------+--------+--------+--------+--------+--------|
      _______, RGB_MOD, RGB_HUI, RGB_SAI, RGB_VAI, _______,                            _______, _______, _______, _______, _______, _______,
  //|--------+--------+--------+--------+--------+--------|                          |--------+--------+--------+--------+--------+--------|
      _______,RGB_RMOD, RGB_HUD, RGB_SAD, RGB_VAD, _______,                            _______, KC_MUTE, KC_VOLU, KC_MNXT, _______, _______,
  //|--------+--------+--------+--------+--------+--------+--------.        ,--------|--------+--------+--------+--------+--------+--------|
      _______, _______, _______, _______, _______, _______, _______,          _______, _______, KC_MPLY, KC_VOLD, KC_MPRV, _______, _______,
  //`--------+--------+--------+----+---+--------+--------+--------/        \--------+--------+--------+---+----+--------+--------+--------'
                                      _______, _______, _______,                  _______, _______, _______
  //                                `--------+--------+--------'                `--------+--------+--------'
  )

//    [_TRNS] = KC_KEYMAP(
//  //,----+----+----+----+----+----.              ,----+----+----+----+----+----.
//         ,    ,    ,    ,    ,    ,                   ,    ,    ,    ,    ,    ,
//  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
//         ,    ,    ,    ,    ,    ,                   ,    ,    ,    ,    ,    ,
//  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
//         ,    ,    ,    ,    ,    ,                   ,    ,    ,    ,    ,    ,
//  //|----+----+----+----+----+----+----.    ,----|----+----+----+----+----+----|
//         ,    ,    ,    ,    ,    ,    ,         ,    ,    ,    ,    ,    ,    ,
//  //`----+----+----+--+-+----+----+----/    \----+----+----+----+----+----+----'
//                           ,    ,    ,             ,    ,
//  //                  `----+----+----'        `----+----+----'
//  ),

};

#ifdef AUDIO_ENABLE
float tone_qwerty[][2]     = SONG(QWERTY_SOUND);
#endif

void persistent_default_layer_set(uint16_t default_layer) {
  eeconfig_update_default_layer(default_layer);
  default_layer_set(default_layer);
}

uint16_t custom_lt_timer;
keypos_t prv_key_pressed;
// Runs just one time when the keyboard initializes.
void matrix_init_user(void) {
//    set_unicode_input_mode(UC_LNX); // Linux
    set_unicode_input_mode(UC_OSX); // Mac OSX
//    set_unicode_input_mode(UC_WIN); // Windows (with registry key, see wiki)
    //set_unicode_input_mode(UC_WINC); // Windows (with WinCompose, see wiki)
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {

  if(record->event.pressed) prv_key_pressed = record->event.key;
  #define IS_KEYPOS_SAME(keyone,keytwo)  ((keyone.col==keytwo.col)&&(keyone.row==keytwo.row))
  #define ANOTHER_KEY_PRESSED (!IS_KEYPOS_SAME(prv_key_pressed, record->event.key))

  inline void tap(uint16_t keycode) {
    register_code(keycode);
    unregister_code(keycode);
  };

  inline void swapInput(void) {
    register_code(KC_LGUI);
    register_code(KC_LALT);
    tap(KC_SPC);
    unregister_code(KC_LALT);
    unregister_code(KC_LGUI);
  };

  switch (keycode) {
    case RASP:
      if (record->event.pressed) {
        custom_lt_timer = timer_read();
        layer_on(2);
        update_tri_layer(_NAVIGATION, _CODE, _ADJUST);
      } else {
        layer_off(2);
        update_tri_layer(_NAVIGATION, _CODE, _ADJUST);
        if (timer_elapsed(custom_lt_timer)<150 && (!ANOTHER_KEY_PRESSED)) {
          register_code(KC_SPC);
          unregister_code(KC_SPC);
        }
      }
      return false;
      break;
    case LOSP:
      if (record->event.pressed) {
        custom_lt_timer = timer_read();
        layer_on(1);
        update_tri_layer(_NAVIGATION, _CODE, _ADJUST);
      } else {
        layer_off(1);
        update_tri_layer(_NAVIGATION, _CODE, _ADJUST);
        if (timer_elapsed(custom_lt_timer)<150 && (!ANOTHER_KEY_PRESSED)) {
          register_code(KC_SPC);
          unregister_code(KC_SPC);
        }
      }
      return false;
      break;
    case TABB:
      if (record->event.pressed) {
        custom_lt_timer = timer_read();
        layer_on(16);
      } else {
        layer_off(16);
        if (timer_elapsed(custom_lt_timer)<150 && (!ANOTHER_KEY_PRESSED)) {
          register_code(KC_TAB);
          unregister_code(KC_TAB);
        }
      }
      return false;
      break;
    case BKAD:
      if (record->event.pressed) {
        custom_lt_timer = timer_read();
        layer_on(16);
      } else {
        layer_off(16);
        if (timer_elapsed(custom_lt_timer)<150 && (!ANOTHER_KEY_PRESSED)) {
          register_code(KC_BSPC);
          unregister_code(KC_BSPC);
        }
      }
      return false;
      break;
    case LOCO:
      if (record->event.pressed) {
        custom_lt_timer = timer_read();
        layer_on(1);
        update_tri_layer(_NAVIGATION, _CODE, _ADJUST);
      } else {
        layer_off(1);
        update_tri_layer(_NAVIGATION, _CODE, _ADJUST);
        if (timer_elapsed(custom_lt_timer)<150 && (!ANOTHER_KEY_PRESSED)) {
          layer_on(_CONSTANTS);
        }
      }
      return false;
      break;
    case FLIP:
      if (record->event.pressed) {
        swapInput();

        register_code(KC_RSFT);
        tap(KC_9);
        unregister_code(KC_RSFT);
        process_unicode((0x256F|QK_UNICODE), record);   // Arm
        process_unicode((0x00B0|QK_UNICODE), record);   // Eye
        process_unicode((0x25A1|QK_UNICODE), record);   // Mouth
        process_unicode((0x00B0|QK_UNICODE), record);   // Eye
        register_code(KC_RSFT);
        tap(KC_0);
        unregister_code(KC_RSFT);
        process_unicode((0x256F|QK_UNICODE), record);   // Arm
        tap(KC_SPC);
        process_unicode((0x0361|QK_UNICODE), record);   // Flippy
        tap(KC_SPC);
        process_unicode((0x253B|QK_UNICODE), record);   // Table
        process_unicode((0x2501|QK_UNICODE), record);   // Table
        process_unicode((0x253B|QK_UNICODE), record);   // Table

        swapInput();
      }
      return false;
      break;
    case TSET: // ┬──┬ ノ( ゜-゜ノ)
      if (record->event.pressed) {
        swapInput();

        process_unicode((0x252C|QK_UNICODE), record);   // Table
        process_unicode((0x2500|QK_UNICODE), record);   // Table
        process_unicode((0x2500|QK_UNICODE), record);   // Table
        process_unicode((0x252C|QK_UNICODE), record);   // Table
        tap(KC_SPC);
        process_unicode((0x30CE|QK_UNICODE), record);   // Arm
        register_code(KC_RSFT);
        tap(KC_9);
        unregister_code(KC_RSFT);
        tap(KC_SPC);
        process_unicode((0x309C|QK_UNICODE), record);   // Eye
        tap(KC_MINS);
        process_unicode((0x309C|QK_UNICODE), record);   // Eye
        process_unicode((0x30CE|QK_UNICODE), record);   // Arm
        register_code(KC_RSFT);
        tap(KC_0);
        unregister_code(KC_RSFT);

        swapInput();
      }
      return false;
      break;
    case SRUG: // ¯\_(ツ)_/¯
      if (record->event.pressed) {
        swapInput();

        process_unicode((0x00AF|QK_UNICODE), record);   // Hand
        tap(KC_BSLS);                                   // Arm
        register_code(KC_RSFT);
        tap(KC_UNDS);                                   // Arm
        tap(KC_LPRN);                                   // Head
        unregister_code(KC_RSFT);
        process_unicode((0x30C4|QK_UNICODE), record);   // Face
        register_code(KC_RSFT);
        tap(KC_RPRN);                                   // Head
        tap(KC_UNDS);                                   // Arm
        unregister_code(KC_RSFT);
        tap(KC_SLSH);                                   // Arm
        process_unicode((0x00AF|QK_UNICODE), record);   // Hand

        swapInput();
      }
      return false;
      break;
    case DISA:       // ಠ_ಠ
      if(record->event.pressed){
        swapInput();

        process_unicode((0x0CA0|QK_UNICODE), record);   // Eye
        register_code(KC_RSFT);
        tap(KC_MINS);
        unregister_code(KC_RSFT);
        process_unicode((0x0CA0|QK_UNICODE), record);   // Eye

        swapInput();
      }
      return false;
      break;
    case END_CONSTANT:
      if (record->event.pressed) {
        SEND_STRING(" = ");
        layer_off(_CONSTANTS);
      }
      return false;
      break;
  }
  return true;
}
