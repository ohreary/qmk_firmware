MIDI_ENABLE = no            # MIDI controls
CONSOLE_ENABLE = no         # Console for debug(+400)
COMMAND_ENABLE = no        # Commands for debug and configuration
RGBLIGHT_ENABLE = no        # Enable WS2812 RGB underlight.
UNICODE_ENABLE = yes

ifndef QUANTUM_DIR
	include ../../../../Makefile
endif
