#include "levinson.h"
#include "action_layer.h"
#include "eeconfig.h"
#include "process_unicode.h"

extern keymap_config_t keymap_config;

// Each layer gets a name for readability, which is then used in the keymap matrix below.
// The underscores don't mean anything - you can have a layer called STUFF or any other name.
// Layer names don't all need to be of the same length, obviously, and you can also skip them
// entirely and just use numbers.

enum levinson_layers {
  _QWERTY,
  _LOWER,
  _NUMBERS,
  _SYMBOLS,
  _CODING,
  _NAVIGATION,
  _ADJUST
};

enum custom_keycodes {
  QWERTY = SAFE_RANGE,
  LOWER,
  RAISE,
  ADJUST,
  SPCNAV,
  SPCNUM,
  SPCSYM,
  SPCCOD
};

// Keycode names
#define SFT_ENT MT(MOD_RSFT, KC_ENT) // Tap for enter, hold for right shift
#define SFT_LEFT MT(MOD_RCTL, KC_LEFT) // Tap for left arrow, hold for right ctrl
#define SFT_UP MT(MOD_RALT, KC_UP) // Tap for up arrow, hold for right alt
#define SFT_CAP MT(MOD_LSFT, KC_CAPS)  // tap for caps, hold for left shift
#define SFT_TAB MT(MOD_LCTL, KC_TAB) // tap for caps, hold for left shift

uint16_t custom_lt_timer;
keypos_t prv_key_pressed;

// Runs just one time when the keyboard initializes.
void matrix_init_user(void) {
//    set_unicode_input_mode(UC_LNX); // Linux
    set_unicode_input_mode(UC_OSX); // Mac OSX
//    set_unicode_input_mode(UC_WIN); // Windows (with registry key, see wiki)
    //set_unicode_input_mode(UC_WINC); // Windows (with WinCompose, see wiki)
};

// Fillers to make layering more clear
#define _______ KC_TRNS
#define XXXXXXX KC_NO

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* Qwerty
 * ,-----------------------------------------.-----------------------------------------.
 * | Esc  |   Q  |   W  |   E  |   R  |   T  |   Y  |   U  |   I  |   O  |   P  | Bksp |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * | Tab  |   A  |   S  |   D  |   F  |   G  |   H  |   J  |   K  |   L  |   ;  |  "   |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |*Shift|   Z  |   X  |   C  |   V  |   B  |   N  |   M  |   ,  |   .  |   /  |*Enter|
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * | Del  |Adjust| Alt  | GUI  |SpcRse|SpcNum|SpcSym|SpcRse| Home | PgDn | PgUp | End  |
 * `-----------------------------------------'-----------------------------------------'
 */
[_QWERTY] = LAYOUT( \
  KC_ESC,  KC_Q,   KC_W,    KC_E,    KC_R,    KC_T,   KC_Y,    KC_U,     KC_I,     KC_O,   KC_P,    KC_BSPC, \
  SFT_TAB, KC_A,   KC_S,    KC_D,    KC_F,    KC_G,   KC_H,    KC_J,     KC_K,     KC_L,   KC_SCLN, KC_QUOT, \
  SFT_CAP, KC_Z,   KC_X,    KC_C,    KC_V,    KC_B,   KC_N,    KC_M,     KC_COMM,  KC_DOT, KC_SLSH, SFT_ENT, \
  KC_DEL,  ADJUST, KC_LALT, KC_LGUI, SPCNUM,  SPCNAV, SPCSYM,  SPCCOD,   KC_HOME, KC_PGUP, KC_PGDN, KC_END \
),

/* Numbers
 * ,-----------------------------------------.-----------------------------------------.
 * |   ~  |   !  |   @  |   #  |   $  |   %  |   ^  |   &  |   *  |   (  |   )  | Del  |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      | Next | Vol- | Vol+ | Play |
 * `-----------------------------------------'-----------------------------------------'
 */

[_NUMBERS] = LAYOUT( \
  KC_GRV,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_DEL, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
  KC_BSPC, _______, _______, _______, _______, _______, _______, _______, KC_MNXT, KC_VOLD, KC_VOLU, KC_MPLY  \
),

/* Symbols
 * ,-----------------------------------------.-----------------------------------------.
 * |   ~  |   1  |   2  |   3  |   4  |   5  |   6  |   7  |   8  |   9  |   0  | Del  |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |     | Next | Vol- | Vol+ | Play |
 * `-----------------------------------------'-----------------------------------------'
 */

[_SYMBOLS] = LAYOUT( \
  KC_GRV,  KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC, KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, KC_DEL, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
  KC_BSPC, _______, _______, _______, _______, _______, _______, _______, KC_MNXT, KC_VOLD, KC_VOLU, KC_MPLY  \
),

/* Coding
 * ,-----------------------------------------.-----------------------------------------.
 * |      |      |      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      | Next | Vol- | Vol+ | Play |
 * `-----------------------------------------'-----------------------------------------'
 */

[_CODING] = LAYOUT( \
  _______, _______, _______, KC_LCBR, KC_RCBR, _______, _______, KC_AMPR, KC_PIPE, _______, _______, KC_DEL, \
  _______, _______, _______, KC_LPRN, KC_RPRN, _______, _______, KC_UNDS, KC_MINS, _______, _______, _______, \
  _______, _______, _______, KC_LBRC, KC_RBRC, _______, _______, KC_PLUS, KC_EQL,  _______, _______, _______, \
  KC_BSPC, _______, _______, _______, _______, _______, _______, _______, KC_MNXT, KC_VOLD, KC_VOLU, KC_MPLY  \
),

/* Navigation
 * ,-----------------------------------------.-----------------------------------------.
 * |      |      |      |  Up  |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      | Left | Down | Right|      |      |      |      |      |      |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      | Next | Vol- | Vol+ | Play |
 * `-----------------------------------------'-----------------------------------------'
 */

[_NAVIGATION] = LAYOUT( \
  _______, _______, _______, KC_UP  , _______, _______, _______, _______, _______, _______, _______, KC_DEL, \
  _______, _______, KC_LEFT, KC_DOWN, KC_RGHT, _______, _______, _______, _______, _______, _______, _______, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
  KC_BSPC, _______, _______, _______, _______, _______, _______, _______, KC_MNXT, KC_VOLD, KC_VOLU, KC_MPLY  \
),

/* Adjust (Lower + Raise)
 * ,-----------------------------------------------------------------------------------.
 * |      |      |      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      |      |      |
 * `-----------------------------------------------------------------------------------'
 */
[_ADJUST] =  LAYOUT( \
  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  \
  _______, BL_STEP, _______, _______, _______, _______, _______, _______, _______, KC_SLEP, KC_PWR,  _______,
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, RESET,   _______, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______ \
)

};

#ifdef AUDIO_ENABLE
float tone_qwerty[][2]     = SONG(QWERTY_SOUND);
#endif

void persistent_default_layer_set(uint16_t default_layer) {
  eeconfig_update_default_layer(default_layer);
  default_layer_set(default_layer);
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {

  if(record->event.pressed)
    prv_key_pressed = record->event.key;

  #define IS_KEYPOS_SAME(keyone,keytwo)  ((keyone.col==keytwo.col)&&(keyone.row==keytwo.row))
  #define ANOTHER_KEY_PRESSED (!IS_KEYPOS_SAME(prv_key_pressed, record->event.key))

  switch (keycode) {
    case QWERTY:
      if (record->event.pressed) {
        #ifdef AUDIO_ENABLE
          PLAY_SONG(tone_qwerty);
        #endif
        persistent_default_layer_set(1UL<<_QWERTY);
      }
      return false;
      break;
    case SPCNUM:
      if (record->event.pressed) {
        custom_lt_timer = timer_read();
        layer_on(_NUMBERS);
        update_tri_layer(_NUMBERS, _SYMBOLS, _ADJUST);
      } else {
        layer_off(_NUMBERS);
        update_tri_layer(_NUMBERS, _SYMBOLS, _ADJUST);
        if (timer_elapsed(custom_lt_timer)<150 && (!ANOTHER_KEY_PRESSED)) {
          register_code(KC_SPC);
          unregister_code(KC_SPC);
        }
      }
      return false;
      break;
    case SPCSYM:
      if (record->event.pressed) {
        custom_lt_timer = timer_read();
        layer_on(_SYMBOLS);
        update_tri_layer(_NUMBERS, _SYMBOLS, _ADJUST);
      } else {
        layer_off(_SYMBOLS);
        update_tri_layer(_NUMBERS, _SYMBOLS, _ADJUST);
        if (timer_elapsed(custom_lt_timer)<150 && (!ANOTHER_KEY_PRESSED)) {
          register_code(KC_SPC);
          unregister_code(KC_SPC);
        }
      }
      return false;
      break;
    case SPCNAV:
      if (record->event.pressed) {
        custom_lt_timer = timer_read();
        layer_on(_NAVIGATION);
      } else {
        layer_off(_NAVIGATION);
        if (timer_elapsed(custom_lt_timer)<150 && (!ANOTHER_KEY_PRESSED)) {
          register_code(KC_SPC);
          unregister_code(KC_SPC);
        }
      }
      return false;
      break;
    case SPCCOD:
      if (record->event.pressed) {
        custom_lt_timer = timer_read();
        layer_on(_CODING);
      } else {
        layer_off(_CODING);
        if (timer_elapsed(custom_lt_timer)<150 && (!ANOTHER_KEY_PRESSED)) {
          register_code(KC_SPC);
          unregister_code(KC_SPC);
        }
      }
      return false;
      break;
    case ADJUST:
      if (record->event.pressed) {
        custom_lt_timer = timer_read();
        layer_on(_ADJUST);
      } else {
        layer_off(_ADJUST);
        if (timer_elapsed(custom_lt_timer)<150 && (!ANOTHER_KEY_PRESSED)) {
          register_code(KC_ENT);
          unregister_code(KC_ENT);
        }
      }
      return false;
      break;
  }
  return true;
}
