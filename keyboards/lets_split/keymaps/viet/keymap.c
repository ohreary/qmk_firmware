#include "lets_split.h"
#include "action_layer.h"
#include "eeconfig.h"

extern keymap_config_t keymap_config;

// Each layer gets a name for readability, which is then used in the keymap matrix below.
// The underscores don't mean anything - you can have a layer called STUFF or any other name.
// Layer names don't all need to be of the same length, obviously, and you can also skip them
// entirely and just use numbers.
#define _QWERTY 0
#define _LOWER 3
#define _RAISE 4
#define _ADJUST 16

enum custom_keycodes {
  QWERTY = SAFE_RANGE,
  LOWER,
  RAISE,
  ADJUST,
  LT_L_SP,
  LT_R_SP
};

// Keycode names
#define SFT_ENT MT(MOD_RSFT, KC_ENT)	// Tap for enter, hold for right shift
#define SFT_RT MT(MOD_RCTL, KC_RGHT) // Tap for right arrow, hold for right ctrl
#define SFT_LT MT(MOD_RALT, KC_LEFT) // Tap for left arrow, hold for right alt
#define SFT_CAP MT(MOD_LSFT, KC_CAPS)	// tap for caps, hold for left shift
#define SFT_DEL MT(MOD_LCTL, KC_DEL)	// tap for caps, hold for left shift

// Fillers to make layering more clear
#define _______ KC_TRNS
#define XXXXXXX KC_NO

// Layout definitions
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* Qwerty
 * ,-----------------------------------------.-----------------------------------------.
 * | Esc  |   Q  |   W  |   E  |   R  |   T  |   Y  |   U  |   I  |   O  |   P  | Bksp |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * | Tab  |   A  |   S  |   D  |   F  |   G  |   H  |   J  |   K  |   L  |   ;  |  "   |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |*Shift|   Z  |   X  |   C  |   V  |   B  |   N  |   M  |   ,  |   .  |   /  |*Enter|
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * | Ctrl |Adjust| GUI  |  Alt |Lower |SpcRse|SpcLow|Raise |*Left |  Up  | Down |*Right|
 * `-----------------------------------------'-----------------------------------------'
 */
[_QWERTY] = KEYMAP( \
  KC_ESC,  KC_Q,   KC_W,    KC_E,    KC_R,  KC_T,   KC_Y,    KC_U,  KC_I,    KC_O,   KC_P,    KC_BSPC, \
  KC_TAB,  KC_A,   KC_S,    KC_D,    KC_F,  KC_G,   KC_H,    KC_J,  KC_K,    KC_L,   KC_SCLN, KC_QUOT, \
  SFT_CAP, KC_Z,   KC_X,    KC_C,    KC_V,  KC_B,   KC_N,    KC_M,  KC_COMM, KC_DOT, KC_SLSH, SFT_ENT, \
  SFT_DEL, ADJUST, KC_LGUI, KC_LALT, LOWER, LT_R_SP, LT_L_SP, RAISE, SFT_LT, KC_UP, KC_DOWN, SFT_RT \
),

/* Lower
 * ,-----------------------------------------.-----------------------------------------.
 * |   `  |   1  |   2  |   3  |   4  |   5  |   6  |   7  |   8  |   9  |   0  | Del  |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * | Tab  |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |   4  |   5  |   6  |   .  |  |   |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |*Shift|  F7  |  F8  |  F9  |  F10 |  F11 |  F12 |   1  |   2  |   3  |   /  |Shift |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * | Ctrl |Adjust| GUI  |  Alt |XXXXXX|SpcRse|SpcLow|      |*Left |  Up  | Down |*Right|
 * `-----------------------------------------'-----------------------------------------'
 */
[_LOWER] = KEYMAP( \
  KC_GRV,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_DEL,
  _______, KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_4 ,   KC_5 ,   KC_6 ,   KC_DOT,  KC_PIPE, \
  _______, KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  KC_1 ,   KC_2 ,   KC_3 ,   _______, _______, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______ \
),

/* Raise
 * ,-----------------------------------------.-----------------------------------------.
 * |   ~  |   !  |   @  |   #  |   $  |   %  |   ^  |   &  |   *  |   (  |   )  | Del  |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * | Tab  |  [   |   ]  |  Up  |   (  |  )   |   {  |   }  |   +  |   =  |      |   \  |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |Shift |Insert| Left | Down |Right |Print |  -   |   _  |  |   |   &  |   /  |Shift |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * | Ctrl |Adjust| GUI  |  Alt |      |SpcRse|SpcLow|XXXXXX| Home | PgUp | PgDn | End  |
 * `-----------------------------------------'-----------------------------------------'
 */
[_RAISE] = KEYMAP( \
  KC_TILD, KC_EXLM, KC_AT,  KC_HASH,  KC_DLR,  KC_PERC, KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, KC_DEL, \
  _______, KC_LBRC, KC_RBRC, KC_UP,   KC_LPRN, KC_RPRN, KC_LCBR, KC_RCBR, KC_PLUS, KC_EQL,  XXXXXXX, KC_BSLS, \
  _______, KC_INS,  KC_LEFT, KC_DOWN, KC_RGHT, KC_PSCR, KC_MINS, KC_UNDS, KC_PIPE, KC_AMPR, _______, _______, \
  _______, _______, _______, _______, _______, _______, _______, _______, KC_HOME, KC_PGUP, KC_PGDN, KC_END \
),

/* Adjust (Lower + Raise)
 * ,-----------------------------------------------------------------------------------.
 * |      |Reset |      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * |      |      |      | Mute | Vol+ | Vol- | Wh Up|LeftC | M-Up |RightC|      |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      | Prev | Play | Next | Wh Dn| M-L  | M-Dn | M-R  |      |      |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      |      |      |
 * `-----------------------------------------------------------------------------------'
 */
[_ADJUST] =  KEYMAP( \
  _______, RESET,   RGB_TOG, _______, _______, _______, _______, _______, _______, _______, KC_SLEP, KC_PWR, \
  _______, RGB_MOD, RGB_RMOD, KC_MUTE, KC_VOLU, KC_VOLD, KC_WH_U, KC_BTN1, KC_MS_U, KC_BTN2, _______, _______, \
  _______, RGB_VAI, RGB_VAD, KC_MPRV, KC_MPLY, KC_MNXT, KC_WH_D, KC_MS_L, KC_MS_D, KC_MS_R, _______, _______, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______ \
)


};

#ifdef AUDIO_ENABLE
float tone_qwerty[][2]     = SONG(QWERTY_SOUND);
#endif

void persistent_default_layer_set(uint16_t default_layer) {
  eeconfig_update_default_layer(default_layer);
  default_layer_set(default_layer);
}

uint16_t custom_lt_timer;
keypos_t prv_key_pressed;

bool process_record_user(uint16_t keycode, keyrecord_t *record) {

  if(record->event.pressed) prv_key_pressed = record->event.key;
  #define IS_KEYPOS_SAME(keyone,keytwo)  ((keyone.col==keytwo.col)&&(keyone.row==keytwo.row))
  #define ANOTHER_KEY_PRESSED (!IS_KEYPOS_SAME(prv_key_pressed, record->event.key))
//  printf("%d", keycode);
  switch (keycode) {
    case QWERTY:
      if (record->event.pressed) {
        #ifdef AUDIO_ENABLE
          PLAY_SONG(tone_qwerty);
        #endif
        persistent_default_layer_set(1UL<<_QWERTY);
      }
      return false;
      break;
    case LOWER:
      if (record->event.pressed) {
        layer_on(_LOWER);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_LOWER);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case RAISE:
      if (record->event.pressed) {
        layer_on(_RAISE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_RAISE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case ADJUST:
      if (record->event.pressed) {
        layer_on(_ADJUST);
      } else {
        layer_off(_ADJUST);
      }
      return false;
      break;
    case LT_L_SP:
      if (record->event.pressed) {
        custom_lt_timer = timer_read();
        layer_on(3);
      } else {
        layer_off(3);
        if (timer_elapsed(custom_lt_timer)<200 && (!ANOTHER_KEY_PRESSED)) {
          register_code(KC_SPC);
          unregister_code(KC_SPC);
        }
      }
      return false;
      break;
    case LT_R_SP:
      if (record->event.pressed) {
        custom_lt_timer = timer_read();
        layer_on(4);
      } else {
        layer_off(4);
        if (timer_elapsed(custom_lt_timer)<200 && (!ANOTHER_KEY_PRESSED)) {
          register_code(KC_SPC);
          unregister_code(KC_SPC);
        }
      }
      return false;
      break;
  }
  return true;
}
